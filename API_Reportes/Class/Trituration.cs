﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace API_Reportes.Class
{
    public class Trituration
    {
        [Key]
        public int ID { get; set; }
        public int IdVariable { get; set; }
        public string DescripcionVariable { get; set; }
        public string DecripcionReporte { get; set; }
        public string AreaVariable { get; set; }
        public string TipoDato { get; set; }
        public string UDM { get; set; }
        public decimal ValorT1 { get; set; }
        public decimal ValorT2 { get; set; }
        public decimal ValorDia { get; set; }
        public DateTime Fecha { get; set; }
        
    }
}
