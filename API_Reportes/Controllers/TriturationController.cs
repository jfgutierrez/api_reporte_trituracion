﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using API_Reportes.Class;
using API_Reportes.Context;
using System.Data;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;

namespace API_Reportes.Controllers
{
    [Route("api/[controller]")]
    public class TriturationController : Controller
    {
        private readonly DataContext context;
        
        public TriturationController(DataContext context) 
        {
            this.context = context;
        }
        
        [HttpGet("getData")]
        public ActionResult getDataByDate(string date)
        {
            try
            {
                var dataTrituration = context.Trituration.FromSqlRaw("EXEC sp_getDataTrituration @date={0}", date).ToList();
               
                return Ok(dataTrituration);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        // GET: TriturationController/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: TriturationController/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: TriturationController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: TriturationController/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: TriturationController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: TriturationController/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: TriturationController/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}
